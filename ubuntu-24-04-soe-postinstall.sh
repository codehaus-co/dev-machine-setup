#!/bin/bash

# ==========================================
# Ubuntu 24.04 Post-Installation Script
# ==========================================
# This script performs additional setup after the system installation:
# - Installs Docker and adds the correct user to the docker group
# - Installs development tools (VS Code, Chrome, Teams, etc.)
# - Configures Citrix Workspace and Rancher Desktop
# - Ensures system security and performs final checks
# ==========================================

# Get the first non-root user (assumes UID 1000 is the first created user)
USERNAME=$(getent passwd 1000 | cut -d: -f1)

echo "Post-install script running as: $(whoami)"
echo "Detected primary user: $USERNAME"

# =========================
# 1. FIX SNAP ISSUES BEFORE INSTALLING
# =========================
echo "Ensuring snapd is installed and running..."
sudo apt update
sudo apt install -y snapd  # Install snapd if missing
sudo systemctl enable --now snapd  # Ensure snap service is running
sudo systemctl restart snapd  # Restart snapd to ensure it's working

# Refresh snap store to get the latest package lists
echo "Refreshing snap store..."
sudo snap refresh

# =========================
# 2. INSTALL DOCKER
# =========================
echo "Installing Docker..."
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "${UBUNTU_CODENAME:-$VERSION_CODENAME}") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# Ensure correct user is added to Docker group
echo "Adding user $USERNAME to the Docker group..."
sudo usermod -aG docker $USERNAME

# =========================
# 3. INSTALL DEVELOPMENT TOOLS
# =========================

# Install Visual Studio Code
echo "Installing Visual Studio Code..."
sudo snap install --classic code || echo "⚠️ Failed to install VS Code"

# Install Google Chrome
echo "Installing Google Chrome..."
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb || sudo apt-get install -f -y

# Install Teams for Linux
echo "Installing Microsoft Teams..."
sudo snap install teams-for-linux || echo "⚠️ Failed to install Microsoft Teams"

# Install Android Studio
echo "Installing Android Studio..."
sudo snap install android-studio --classic || echo "⚠️ Failed to install Android Studio"

# =========================
# 4. INSTALL RANCHER DESKTOP
# =========================
echo "Installing Rancher Desktop..."
curl -s https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/Release.key | gpg --dearmor | sudo dd status=none of=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg
echo 'deb [signed-by=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg] https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/ ./' | sudo dd status=none of=/etc/apt/sources.list.d/isv-rancher-stable.list
sudo apt update
sudo apt install -y rancher-desktop

# Set the screen blank timeout to 5 minutes (300 seconds)
gsettings set org.gnome.desktop.session idle-delay 300

# Set the lock timeout to 5 minutes (300 seconds)
gsettings set org.gnome.desktop.screensaver lock-delay 300

# Ensure automatic screen locking is enabled
gsettings set org.gnome.desktop.screensaver lock-enabled true

echo "Screen timeout and lock timeout set to 5 minutes."

# =========================
# 5. FINAL SYSTEM CHECKS
# =========================
echo "Running final system checks..."
sudo systemctl status docker
sudo systemctl status snapd
sudo systemctl status apparmor

echo "Ubuntu 24.04 Workstation setup complete."

# =========================
# INSTALL CITRIX WORKSPACE
# =========================
echo "You need to manually install Citrix Workspace..."
# Download manually: https://www.citrix.com/downloads/workspace-app/legacy-workspace-app-for-linux/workspace-app-for-linux-latest-2408.html
# Save as citrix.deb
# Run the beloe to check checksum:

############

#expected_checksum="b2c9e48f21a0c576d9b24dc44c23ba138d719a9ad8f4a37a0be980a0866ab8d2"
#calculated_checksum=$(sha256sum "citrix.deb" | awk '{print $1}')

#if [ "$calculated_checksum" = "$expected_checksum" ]; then
#    echo "Checksum verified, installing Citrix..."
#    sudo apt install -f ./citrix.deb
#else
#    echo "Checksum failed, Citrix installation aborted."
#fi
######################

