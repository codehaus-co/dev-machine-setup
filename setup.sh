#!/bin/bash

# Update APT package index
echo "Updating APT package index..."
sudo apt-get update

# Install packages to allow APT to use a repository over HTTPS
echo "Installing prerequisites for Docker and others..."
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
    python3-pip \
    python3-venv

# Install net-tools
echo "Installing net-tools..."
sudo apt-get install -y net-tools

# Add Docker's official GPG key
echo "Adding Docker's official GPG key..."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the stable Docker repository
echo "Setting up the stable Docker repository..."
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Update the APT package index again with Docker packages from the newly added repo
echo "Updating APT package index again..."
sudo apt-get update

# Install the latest version of Docker CE
echo "Installing the latest version of Docker CE..."
sudo apt-get install -y docker-ce

# Verify Docker CE is installed correctly by running the hello-world image
echo "Verifying Docker CE installation..."
sudo docker run hello-world

# Install Git
echo "Installing Git..."
sudo apt-get install -y git

# Install Virtualbox
echo "Installing Virtualbox..."
sudo apt-get install -y install virtualbox

# Download and install Google Chrome
echo "Downloading and installing Google Chrome..."
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get install -f -y # Fix possible missing dependencies

# Install Visual Studio Code
echo "Installing Visual Studio Code..."
sudo snap install --classic code

# Install pipx
#echo "Installing pipx..."
#sudo apt install pipx
#pipx ensurepath


# Install GitMan with pipx
#echo "Installing GitMan with pipx..."
#pipx install gitman
#pipx ensurepath

# Install Teams
snap install teams-for-linux

# Install Android Studio
sudo snap install android-studio --classic

# Install Citrix Workstation

# Download.
wget -O citrix.deb  https://downloads.citrix.com/22629/icaclient_24.2.0.65_amd64.deb?__gda__=exp=1712534214~acl=/*~hmac=e48f243ade68e6c1e01da01e1dfb12f8317efa222df78a3ea864b15abac0c507

# Define the path to the downloaded file
downloaded_file="./citrix.deb"

# Define the expected checksum (provided as a string)
expected_checksum="0bc1fd3e59583c17021d0710c72251ac2d6c8bc6fd8f8b93987add8301ebc004"

# Calculate the checksum of the downloaded file
calculated_checksum=$(sha256sum "$downloaded_file" | awk '{print $1}')

# Compare the calculated checksum with the expected checksum
if [ "$calculated_checksum" = "$expected_checksum" ]; then
    echo "Checksums match. The file is intact, installing Citrix..."
    apt install -f ./citrix.deb
else
    echo "Checksums do not match. The file may be corrupted or altered."
fi

# Install flameshot for screenshots
apt install flameshot

# OPTIONAL: Add current user to the Docker group to run Docker commands without sudo
echo "Adding current user to the Docker group..."
sudo usermod -aG docker ${USER}

# Install Rancher Desktop
curl -s https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/Release.key | gpg --dearmor | sudo dd status=none of=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg
echo 'deb [signed-by=/usr/share/keyrings/isv-rancher-stable-archive-keyring.gpg] https://download.opensuse.org/repositories/isv:/Rancher:/stable/deb/ ./' | sudo dd status=none of=/etc/apt/sources.list.d/isv-rancher-stable.list
apt update
apt install rancher-desktop

echo "Installation complete. Please logout and log back in for the Docker group change and PATH update to take effect."
